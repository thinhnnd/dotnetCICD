@echo off
cls

set APP_NAME=dotNetCICD
set DIST_PATH="C:\Distribution\dotNetCICD"
set PORT=9001

setx path "%windir%\system32\inetsrv"

IF NOT EXIST %DIST_PATH% (
    mkdir %DIST_PATH% 
) 