#!/bin/bash

APP="dotNetCICD"
DIST_PATH="C:\Distribution\dotNetCICD"
FULL_PATH="C:\Distribution\dotNetCICD\dotNetCICD.exe"

if [ -e $DIST_PATH ]
then 
    sc stop $APP
    echo "RUN: sc stop $APP"
else 
    mkdir $DIST_PATH
    sc create $APP binPath=$FULL_PATH
    echo "RUN: sc create $APP binPath=$FULL_PATH"
fi;